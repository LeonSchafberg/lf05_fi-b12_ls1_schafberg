
public class KlasseAufgabenformatierungErgebnissN {

	public static void main(String[] args) {
		// Aufgabe 2 
		// Variante 1 
		
		System.out.printf("%-5s","0!");
		System.out.printf("%s","=");
		System.out.printf("%-19s","");
		System.out.printf("%s","=");
		System.out.printf("%4s%n","1");
		
		System.out.printf("%-5s","1!");
		System.out.printf("%s","=");
		System.out.printf("%-19s"," 1");
		System.out.printf("%s","=");
		System.out.printf("%4s%n","1");
		
		System.out.printf("%-5s","2!");
		System.out.printf("%s","=");
		System.out.printf("%-19s"," 1 * 2");
		System.out.printf("%s","=");
		System.out.printf("%4s%n","2");
		
		System.out.printf("%-5s","3!");
		System.out.printf("%s","=");
		System.out.printf("%-19s"," 1 * 2 * 3");
		System.out.printf("%s","=");
		System.out.printf("%4s%n","6");
		
		System.out.printf("%-5s","4!");
		System.out.printf("%s","=");
		System.out.printf("%-19s"," 1 * 2 * 3 * 4");
		System.out.printf("%s","=");
		System.out.printf("%4s%n","24");
		
		System.out.printf("%-5s","5!");
		System.out.printf("%s","=");
		System.out.printf("%-19s"," 1 * 2 * 3 * 4 * 5");
		System.out.printf("%s","=");
		System.out.printf("%4s%n","120");
		
		// Variante 2 Noch nicht fertiggestellt
		
		System.out.printf("%-5s %s %-19s %s %s %n","1!","=","1","=","1");
		System.out.printf("%-5s %-19s %4s %s %n","1!","=","=","1");
		System.out.printf("%-5s %-19s %4s %s %n","2!","=","=","2");
		System.out.printf("%-5s %-19s %4s %s %n","3!","=","=","6");
		System.out.printf("%-5s %-19s %4s %s %n","4!","=","=","24");
		System.out.printf("%-5s %-19s %4s %s %n","5!","=","=","120");
		

		
	}

}
