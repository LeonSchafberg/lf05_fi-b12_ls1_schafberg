import java.util.Scanner;

public class Methoden {

	public static void main(String[] args) {

		starteBestellungErfassen();

	}

	public static void starteBestellungErfassen() {
		double zuZahlenderBetrag = fahrkartenbestellungErfassen(); // Wie viel muss insgesamt bezahlt werden?
		double fahrkarteBezahlen = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		double rueckgabe = rueckgeldAusgeben(zuZahlenderBetrag, fahrkarteBezahlen);
		muenzeAusgeben(rueckgabe);
	}

	public static double fahrkartenbestellungErfassen() {

		double zuZahlenderBetrag;
		double anzahlDerTickets;

		Scanner tastatur = new Scanner(System.in);

		System.out.println("\nW�hlen Sie ihre Wunschfahrkarte aus:\n" + " => Einzelfahrausweis Berlin ABC [3.80] (1)\n"
				+ " => Einzelfahrausweis Berlin AB [3.00] (2)\n" + " => Einzelfahrausweis Berlin BC [3.50] (3)");
		zuZahlenderBetrag = tastatur.nextDouble();
		if (zuZahlenderBetrag == 1) { // BerlinABC
			zuZahlenderBetrag = 3.80;
		}
		if (zuZahlenderBetrag == 2) { // BerlinAB
			zuZahlenderBetrag = 3.00;
		}
		if (zuZahlenderBetrag == 3) { // BerlinBC
			zuZahlenderBetrag = 3.50;
		}

		System.out.println("Anzahl der Tickets: ");
		anzahlDerTickets = tastatur.nextDouble();
		if (anzahlDerTickets <= 0 || anzahlDerTickets > 10) {

			anzahlDerTickets = 1;
			System.out.println("Eingabe ung�ltig! Die Anzahl der Tickets wurde auf 1 gesetzt");
		}

		zuZahlenderBetrag = zuZahlenderBetrag * anzahlDerTickets;

		return zuZahlenderBetrag;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfeneM�nze;
		double eingezahlterGesamtbetrag;

		Scanner tastatur = new Scanner(System.in);

		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static double rueckgeldAusgeben(double zuZahlenderBetrag, double fahrkarteBezahlen) {
		double r�ckgabebetrag;

		r�ckgabebetrag = fahrkarteBezahlen - zuZahlenderBetrag;

		return r�ckgabebetrag;

	}

	public static void muenzeAusgeben(double rueckgabe) {

		if (rueckgabe > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro\n", rueckgabe);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabe >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				rueckgabe -= 2.0;
			}
			while (rueckgabe >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				rueckgabe -= 1.0;
			}
			while (rueckgabe >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				rueckgabe -= 0.5;
			}
			while (rueckgabe >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				rueckgabe -= 0.2;
			}
			while (rueckgabe >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				rueckgabe -= 0.1;
			}
			while (rueckgabe >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				rueckgabe -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");

		starteBestellungErfassen();
	}

}