
public class AusgabenformatierungBeispiele {

	public static void main(String[] args) {
		// Ganzzahl
		//System.out.printf("|%-10d|",123456, -123456);
		
		// Kommazahlen 
		//System.out.printf("|%+-10.2f|",12.123456789);
		
		// Zeichenketten
		
		//System.out.printf("|%-10.3s|","Max Mustermann");
		
		System.out.printf("Name:%1"+ "0s Alter:%8d	Gewicht:%10.2f", "Max",28, 80.50);
	
		//System.out.printf("%-19s","1*2*3");
	}

}
