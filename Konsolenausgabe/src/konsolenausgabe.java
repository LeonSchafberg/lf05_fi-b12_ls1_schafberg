
public class konsolenausgabe {

	public static void main(String[] args) {
		
	//Aufgabe 1 
		//print=Alles in eine Zeile
		//prinln= 1. Befehl ist in 1. Zeile
		//2. Befehl ist in der 2. Zeile der Console
		System.out.print("Das ist der erste Satze.\t");
		System.out.print("Das ist der zweite Satz.");
		
	
	//Aufgabe 3
		double d = 22.4234234;
		System.out.printf( "|%.2f| |%.2f|\n" , d, -d);
		double e = 111.2222;
		System.out.printf( "|%.2f| |%.2f|\n" , e, -e);
		double f = 4.0;
		System.out.printf( "|%.2f| |%.2f|\n" , f, -f);
		double g = 1000000.551;
		System.out.printf( "|%.2f| |%.2f|\n" , g, -g);
		double h = 97.34;
		System.out.printf( "|%.2f| |%.2f|\n" , h, -h);
	}

}
